package test.test.com.tinder.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("api/feeds")
    Call<ResponseBody> getFeeds();
}
